import 'package:flutter/material.dart';
import 'package:map_launcher_ohos/map_launcher_ohos.dart';

import 'maps_sheet.dart';

class ShowMarker extends StatefulWidget {
  const ShowMarker({super.key});

  @override
  State<ShowMarker> createState() => _ShowMarkerState();
}

class _ShowMarkerState extends State<ShowMarker> {
  double latitude = 22.592429967609824;
  double longitude = 114.30795419058587;
  String title = '大梅沙海滨公园';
  int zoom = 18;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          TextFormField(
            autocorrect: false,
            autovalidateMode: AutovalidateMode.disabled,
            decoration: const InputDecoration(labelText: 'Latitude'),
            initialValue: latitude.toString(),
            onChanged: (newValue) {
              setState(() {
                latitude = double.tryParse(newValue) ?? 0;
              });
            },
          ),
          TextFormField(
            autocorrect: false,
            autovalidateMode: AutovalidateMode.disabled,
            decoration: const InputDecoration(labelText: 'Longitude'),
            initialValue: longitude.toString(),
            onChanged: (newValue) {
              setState(() {
                longitude = double.tryParse(newValue) ?? 0;
              });
            },
          ),
          TextFormField(
            autocorrect: false,
            autovalidateMode: AutovalidateMode.disabled,
            decoration: const InputDecoration(labelText: 'Title'),
            initialValue: title,
            onChanged: (newValue) {
              setState(() {
                title = newValue;
              });
            },
          ),
          MaterialButton(
            onPressed: () {
              MapsSheet.show(
                context: context,
                onMapTap: (map) {
                  map.showMarker(
                    coords: Coords(latitude, longitude),
                    title: title,
                    zoom: zoom,
                  );
                },
              );
            },
            child: const Text('Show Maps'),
          )
        ],
      ),
    );
  }
}
